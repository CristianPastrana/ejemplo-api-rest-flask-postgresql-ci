# API REST

## Objetivo

Explicar de manera breve como levantar un servicio (API) utilizando contenedores.

## ¿Qué vamos a utilizar?

- Python 3.9
- Flask
- PostgreSQL
- Docker
- Docker compose
- Gitlab
- Postman
- Pgadmin

## ¿Que tengo que hacer?

### Clonar el proyecto a un directorio que nos quede comodo.
>```sh
>$ git clone https://gitlab.com/CristianPastrana/ejemplo-api-rest-flask-postgresql-ci.git
>```
### Instalar Docker y Docker compose
En Linux :
- Docker : seguir los pasos de este [enlace](https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es) . (solo la parte de instalacion)
- Docker compose : seguir los pasos de este [enlace](https://www.digitalocean.com/community/tutorials/como-instalar-docker-compose-en-ubuntu-18-04-es) .

En windows:

Instalar [Docker-Desktop](https://www.docker.com/products/docker-desktop) . En el paquete de instalacion ya esta incluido Docker compose.

### Creando imagen y contenedores

Para crear la imagen debemos estar situados en el directorio del proyecto y ejecutamos:
>```sh
>$ docker-compose build
>```
Con este comando habremos creado la imagen de la API, esta imagen se buildea de acuerdo a lo que hayamos puesto en el dockerfile.
>```sh
>#Elegimos la imagen que corresponde a la version de python que vamos a utilizar
>FROM python:3.9
>#Creamos un directorio de trabajo en la imagen
>WORKDIR /code
>#Copiamos todos los archivos de nuestro proyecto al directorio de trabajo (menos los que esten en el .dockerignore )
>COPY . /code/
>#Instalamos las dependencias de nuetro proyecto
>RUN pip install --no-cache-dir -r requirements.txt
>#Ejecutamos el comando que iniciara nuestro servicio
>CMD [ "python", "server.py" ]
>```

Si todo fue bien habremos recibido un mensaje: "Successfully built ....."

El siguiente paso es crear los contenedores, para ellos ejecutamos:

>```sh
>$ docker-compose up
>```

veremos algo del estilo:

```
Starting movies-db ... done
Starting movies-api ... done
Attaching to movies-db, movies-api
movies-db    |
movies-db    | PostgreSQL Database directory appears to contain a database; Skipping initialization
movies-db    |
movies-db    | 2021-01-06 21:32:45.682 UTC [1] LOG:  starting PostgreSQL 13.1 (Debian 13.1-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit    
movies-db    | 2021-01-06 21:32:45.682 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
movies-db    | 2021-01-06 21:32:45.682 UTC [1] LOG:  listening on IPv6 address "::", port 5432
movies-db    | 2021-01-06 21:32:45.688 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
movies-db    | 2021-01-06 21:32:45.695 UTC [26] LOG:  database system was shut down at 2021-01-06 21:32:41 UTC
movies-db    | 2021-01-06 21:32:45.701 UTC [1] LOG:  database system is ready to accept connections
movies-api   |  * Serving Flask app "src" (lazy loading)
movies-api   |  * Environment: production
movies-api   |    WARNING: This is a development server. Do not use it in a production deployment.
movies-api   |    Use a production WSGI server instead.
movies-api   |  * Debug mode: off
movies-api   | INFO:werkzeug: * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```
Con esto, la base de datos quedará expuesta en el puerto 5432 y la API en el 5000.

### Iniciando o deteniendo contenedores
Una vez que los contenedores están creados, se puede detener la ejecución de ambos, con:


>```sh
>$ docker-compose stop
>```


Y volver a iniciarlas con

>```sh
>$ docker-compose start
>```


### Ver los contenedores que estan activos
Para ver que contenedores estan funcionando, ejecutamos:
>```sh
>$ docker ps
>```

veremos lo siguiente:
```
CONTAINER ID   IMAGE       COMMAND                  CREATED             STATUS         PORTS                    NAMES
bb7feb62fafc   movie-api   "python server.py"       About an hour ago   Up 6 minutes   0.0.0.0:5000->5000/tcp   movies-api
5d3fdc897540   postgres    "docker-entrypoint.s…"   5 hours ago         Up 6 minutes   0.0.0.0:5432->5432/tcp   movies-db
```
### Eliminar un contenedor
Para eliminar un contenedor primero debemos detenerlo(anque tambien se puede eliminar en caliente) y luego ejecutamos:

>```sh
>$ docker rm <CONTAINDER_ID>
>```
donde el CONTAINDER_ID es el mismo que vimos en el paso anterior.

## ¿Que es lo que hace nuestra API?
Nuestro servicio tiene como tarea crear peliculas que seran almacenadas en una base de datos y ademas tambien debe poder consultarse cada una de ellas mediante un ID.
Para el desarrollo se utilizo Python, Flask como framework, sqlAlchemy como ORM y PostgreSQL para la base de datos. 

## ¿Como puedo probar la API?
Para relizar las pruebas de llamadas a la API, utilizaremos Postman. Entonces lo siguiente sera descargarlo e instalarlo.

En Linux :
- Seguir los pasos de este [enlace](https://geeksencuarentena.com/linux/como-instalar-postman-en-el-escritorio-de-linux/) . 

En windows:

- Desde este [enlace](https://www.postman.com/downloads/) .

Una vez instalado, abrimos la aplicacion e importamos la colección de Postman que se encuentra en el directorio del mismo nombre. 

Si todo esta bien, veremos la coleccion en la columna izquierda.

![1](/uploads/6b82c570d63923af07482127560e3338/1.PNG)


Seleccionamos "Movie- Crear pelicula" y se abrira la siguiente pestaña:

![2](/uploads/0fe9d2afad296af610fca399573de797/2.PNG)



Clickeamos en "Send" para enviar la solicitud y obtendremos la respuesta:

![3](/uploads/73e5b59f5930f10099700bb7cc925805/3.PNG)

Con esta peticion habremos creado una pelicula en la base de datos.
Ahora puedo buscarla por su id mediante la otra peticion "Movie - Buscar por ID".

![4](/uploads/23c27c8e2be8494d9bbb7cb04cc81dc1/4.PNG)

Clickeamos en "Send" para enviar la solicitud y obtendremos la respuesta:

![5](/uploads/8eea26b700aed641a7a289ece9d9ec0c/5.PNG)

# OPCIONAL
## PGADMIN

Para validar los datos cargados en la base de datos podemos usar pgadmin como cliente de conexion.

### Como acceder

URL: http://localhost:5050

Username: pgadmin4@pgadmin.org

Password: admin
