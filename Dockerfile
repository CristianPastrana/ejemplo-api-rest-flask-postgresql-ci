#elegimos la imagen que corresponde a la version de python que vamos a utilizar
FROM python:3.9

#Creamos un directorio de trabajo en la imagen
WORKDIR /code

#Copiamos todos los archivos de nuestro proyecto al directorio de trabajo (menos los que esten en el .dockerignore )
COPY . /code/

#Instalamos las dependencias de nuetro proyecto
RUN pip install --no-cache-dir -r requirements.txt

#Ejecutamos el comando que iniciara nuestra aplicacion
CMD [ "python", "server.py" ]
