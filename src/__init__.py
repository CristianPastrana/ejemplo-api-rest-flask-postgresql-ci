from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def create_app(config="Config"):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object("config.{}".format(config))
    from .routes import movies, route_error  # Import routes
    app.register_blueprint(movies)
    route_error(app)
    db.init_app(app)

    return app
