from flask import Blueprint, request
from . import utls
import logging
from .services import movieService
from .repositories import movieRepository
from .exceptions import BusinessException
from .dtos import MovieDTO

logging.basicConfig(level=logging.INFO)
movies = Blueprint("movie-api", __name__, url_prefix="/movie/")



@movies.route("/",methods=['POST'])
@utls.only_json
def createMovie():
    newMovie = movieService.create(request.json_data)
    return utls.responseJson(newMovie, 201)

@movies.route("/<movie_id>",methods=['GET'])
def getMovie(movie_id):
    movie = movieRepository.getById(movie_id)
    if(movie is None):
        return utls.responseNotFound("No se encontró la pelicula")
    return utls.responseJson(MovieDTO(movie).serialize())





def route_error(app):
    @app.errorhandler(Exception)
    def exception_handler(e):
        logging.error(str(e))
        return utls.response_error(str(e), 500)

    @app.errorhandler(BusinessException)
    def business_exception_handler(e):
        return utls.responseBusinessError(e.message)