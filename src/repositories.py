from sqlalchemy import extract, func
from .models import *
from . import db

class MovieRepository():
    def __query(self, title = None, order_by = None):
        query = Movie.query
        if(not title is None):
            query = query.filter(Movie.title==title)
        if(not order_by is None):
            query = query.order_by(order_by)          
        return query
    
    def getById(self, movie_id):
        return Movie.query.get(movie_id)
        
    def saveChanges(self, entities):
        for entity in entities:
            db.session.add(entity)
        db.session.commit()

    def count(self, title = None):
        return self.__query(title=title).count()

    def find(self, title = None, actor = None, category = None, order_by = None):
        return self.__query(title=title, order_by=order_by).all()

    

movieRepository = MovieRepository()