from .models import Movie
from .repositories import movieRepository
from . import utls
from .dtos import MovieDTO
from .exceptions import MovieTitleAlreadyExists

class MovieService():
    
    def create(self, dataJson):
        movieSearchedByTitleCount = movieRepository.count(title=dataJson.get("title"))

        if movieSearchedByTitleCount  > 0:
            raise MovieTitleAlreadyExists

        newMovie = Movie(dataJson.get("title"), dataJson.get("category"), dataJson.get("actor"), dataJson.get("price"))
        movieRepository.saveChanges([newMovie])

        return MovieDTO(newMovie).serialize()
    

movieService = MovieService()